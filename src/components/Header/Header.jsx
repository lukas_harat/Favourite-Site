import React from 'react';
import { Accordion } from 'chayns-components';

const headder = () => {
    return (
        <div>
            <h1>Die Community Favourites</h1>
            <Accordion head={'Ist deine Site nicht hier ?'}>
                <div className="tapp">
                    <div className="content__card">
                        <div className="question_answer">Dein Name</div>
                        <textarea className="input"></textarea>
                    </div>
                    <div className="content__card">
                        <div className="question_answer">Deine Adresse</div>
                        <textarea className="input"></textarea>
                    </div>
                    <div className="content__card">
                        <div className="question_answer">
                            Deine eMail Adresse
                        </div>
                        <textarea className="input"></textarea>
                    </div>
                    <div className="content__card">
                        <div className="question_answer">Kommentar</div>
                        <textarea className="input"></textarea>
                    </div>
                </div>
            </Accordion>
        </div>
    );
};

export default headder;
