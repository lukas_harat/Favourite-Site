module.exports = {
    development: {
        host: '0.0.0.0',
        port: 8080,
        cert: '\\\\fs1.tobit.ag\\ssl\\tobitag.crt',
        key: '\\\\fs1.tobit.ag\\ssl\\tobitag.key',
    },
    output: {
        singleBundle: false,
        filename: '[package].[contenthash].js',
    },
    webpack(config, { webpack, dev }) {
        // edit webpack config here
        return config;
    },
};
